# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.11.4] - 2025-03-01

### Fixed
- Fixed `configtool.openmw:list_dir`. It was not behaving correctly unless the VFS env variable was set
  something left over from portmod's deprecated builtin VFS

## [0.11.3] - 2025-02-14

### Fixed
- Fixed using `configtool.openmw:find_file` to search for files in archives
- find_file will no longer print messages when extracting from bsa archives

## [0.11.2] - 2023-03-30

### Changed
- The `path` attribute in config files is now optional,
  allowing config files to only affect settings/fallback.

### Fixed
- Made `INSTALL_DEST` in legacy code be case-insensitive

## [0.11.1] - 2023-02-26

### Fixed
- Fixed regression when the openmw config directory doesn't exist.

## [0.11.0] - 2023-02-14

### Dependencies
- tomli is required except on python 3.11 and newer

### Added
- Configuration files can now be toml.
- In WSL2 (if `WSL_INTEROP` is set), data directory paths will be converted to Windows-style paths.
  This works by replacing `/mnt/c/` with `c:/` (and the same for other drive letters).

### Changed
- Raise readable FileNotFoundError when files referenced in config files do not exist.

### Fixed
- Will no longer throw an exception if the openmw config directory does not exist.
- Fixed flag-specific overrides for new-style config files always being applied regardless of
  whether the flag matches.

## [0.10.1] - 2023-01-17
### Fixed
- Made flag requirements for sorting rules from Pybuild2 packages only use the config files
  not the package metadata.

## [0.10.0] - 2023-01-16

### Added
- Added `configtool.openmw:find_file` and `configtool.openmw.list_dir` for probing the OpenMW VFS
  as a replacement for portmod's builtin which works with the new VFS.
  This works by generating the VFS on the fly, rather than parsing it from openmw.cfg.

### Fixed
- Fixed bug causing Pybuild2 packages to sometimes attempt to be added to the VFS/config.
- Fixed bug where user fallback.cfg/settings.cfg would always provide lowercase keys.
- Fixed bug where tiebreakers for install directory size orderings wasn't working, resulting
  in arbitrary changes between directories of the same size
- Fixed new-style config directories from being excluded from orderings

## [0.9.0] - 2022-09-05

### Dependencies
- DeltaPlugin 0.16 or newer is now required.

### Changed
- Plugins and Data directories are now ordered by size, starting with the largest
  - Data directory size is defined by the number of files.
  - Plugin size is defined by the number of records.

### Fixed
- Fixed bug causing "local" mods with uppercase directories to be ignored (#9).

## [0.8.0] - 2022-07-13

### Added
- Added support for portmod 2.4. Portmod 2.3 is also still supported.

### Fixed
- Fixed the error when a plugin is missing and you have `TIMESTAMP_PLUGINS` set in bethesda.pmodule
- Fixed bug causing the bethesda VFS to ignore `DATA_OVERRIDES`.
- bethesda VFS plugins are now looked up in a case-insensitive manner, and must end in `-vfs` to
  try to ensure they are unique.
- Fixed case insensitivity of user sorting rules

## [0.7.1] - 2022-06-03

### Fixed
- Fixed issue with using configtool with the current version of portmod.

## [0.7.0] - 2022-06-03

### Added
- Added support for a json-based config file format as an alternative for defining the VFS
- Added `PLUGINS_TXT_FILE_PREFIX` to `bethesda.pmodule`, to allow customizing the plugins.txt output

### Fixed
- OpenMW's `settings.cfg` will now be created if it does not already exist.

## [0.6.5] - 2022-05-18

### Fixed
- It is no longer possible to use configtool to modify the global openmw.cfg (only the user one).
  This relies on the first line of that file containing the message "do not modify".

## [0.6.4] - 2022-05-15

Added installation support for fallout-4 via the bethesda entry point

## [0.6.3] - 2021-12-08

### Fixes
- Fixed handling of openmw.cfg and settings.cfg when they are symlinks. The file the symlink
  points to will be replaced instead of the symlink itself.

## [0.6.2] - 2021-11-06

### Fixes
- Fixed absolute path in setup.py breaking installation on Windows
- Fixed broken function call in bethesda module

## [0.6.1] - 2021-09-01

### Fixes
- Import failure caused by renaming to configtool


## [0.6.0] - 2021-08-31

### Added
- Added bethesda entry point for use with Fallout, Oblivion and Skyrim.
- pmodule files are now installed via setuptools (which is now a build dependency)

### Changed
- [DeltaPlugin](https://gitlab.com/bmwinger/delta-plugin) is now a dependency
  (used for detecting plugin master files, to support all esp versions).
- ConfigObj has been replaced with [roundtripini](https://gitlab.com/bmwinger/roundtripini)
  for better support of the various config files.

### Fixes
- Duplicate sections in openmw.cfg are now supported and won't cause errors
- Plugin masters which have a different case than the file in the filesystem are now recognized

## [0.5.0] - 2021-05-21

### Added

- Added comment support to openmw.cfg. Comments will now be preserved
- Supports portmod as of portmod!339. Does not support prior versions.

### Fixed

- Clarified warning message about missing files in user configuration.
- Fixed preservation of comments in settings.cfg via use of ConfigObj

## [0.4.0] - 2021-04-05

### Added
- Restructured for new portmod filesystem

## [0.3.2] - 2021-02-14

### Fixed
- Fixed bug causing crash in 0.3.1

## [0.3.1] - 2021-02-13

### Fixed
- Fixed splitting of the OPENMW_CONFIG_DIR on Windows

## [0.3.0] - 2021-02-12

### Added
- Added support for groundcover= entries in openmw.cfg. These should be added as
  files in the GROUNDCOVER category.
- Added support for user configurations for fallback entries in openmw.cfg (via
  fallback.cfg in portmod's <prefix>/config directory) and settings.cfg (via settings.cfg
  in portmod's <prefix>/config directory). Note that modifications to settings.cfg
  are still not destructive, so you can always add new settings manually, however
  to change one that is modified by portmod, you will need to use portmod's
  settings.cfg file.

## [0.2.1] - 2021-02-08

### Fixed
- Uses the default config directory (the first listed in OPENMW_CONFIG_DIR) if none of the directories exist.

## [0.2.0] - 2020-11-25

### Added
- Updated for portmod 2.0_rc0
- Added support for an OPENMW_CONFIG_DIR environment variable, which can take the form of a colon-separated path, which can be used in place of the older OPENMW_CONFIG and OPENMW_SETTINGS variables.
