import os

from setuptools import setup

modules = []
if os.environ.get("ARCH") == "openmw":
    modules.append("configtool.openmw.pmodule")
elif os.environ.get("ARCH") in ("oblivion", "fallout-nv", "skyrim", "fallout-4"):
    modules.append("configtool.bethesda.pmodule")

setup(
    name="configtool",
    version="0.11.4",
    description="Portmod module for updating game config files",
    author="Benjamin Winger",
    author_email="bmw@disroot.org",
    url="https://gitlab.com/portmod/configtool",
    packages=["configtool"],
    install_requires=["roundtripini>=0.3", 'tomli >= 1.1.0 ; python_version < "3.11"'],
    data_files=[("modules", modules)],
)
