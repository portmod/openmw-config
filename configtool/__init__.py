# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""Module for updating and sorting openmw config sections"""

import csv
import glob
import json
import os
import re

try:
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib
from collections import defaultdict
from functools import lru_cache
from logging import warning
from typing import Dict, List, NamedTuple, Optional, Set, Tuple, Union, cast

from portmod.globals import env
from portmod.loader import (
    load_all_installed,
    load_all_installed_map,
    load_installed_pkg,
)
from portmodlib.atom import Atom, atom_sat
from portmodlib.execute import execute
from portmodlib.usestr import use_reduce
from roundtripini import INI

from .configfile import remove_config
from .files import get_dir_path, get_directories, get_file_path, get_files
from .tsort import CycleException, tsort

ROOT = os.environ["ROOT"]


class File(NamedTuple):
    name: str
    overrides: List[str] = []


class ConfigData(NamedTuple):
    id: Atom
    """
    Package name and category identifying this entry

    Must parse as an atom
    """
    path: Optional[str] = None
    """
    Absolute path to the directory this VFS entry refers to
    If empty this config file will not affect the VFS
    """
    groundcover: List[File] = []
    plugins: List[File] = []
    archives: List[File] = []
    tier: Union[str, int] = "a"
    overrides: List[Atom] = []
    flags: List[str] = []
    fallback: Optional[Dict[str, Dict[str, str]]] = None
    settings: Optional[Dict[str, Dict[str, str]]] = None


def _map_data(jsondata: Dict):
    result = {}
    for key, value in jsondata.items():
        if key == "id":
            result["id"] = Atom(value)
        elif key in ("groundcover", "plugins", "archives"):
            result[key] = [File(**file) for file in value]
        elif key == "overrides":
            result[key] = [Atom(atom) for atom in value]
        elif key == "tier":
            # Allow integers, but parse as strings
            result[key] = str(value)
        else:
            result[key] = value
    return result


@lru_cache()
def get_config_files() -> List[ConfigData]:
    results = []
    configpath = os.environ.get("CONFIGPATH")
    if configpath:
        for path in glob.iglob(
            os.path.join(ROOT, configpath, "configtool", "**/*.json"), recursive=True
        ):
            with open(path) as file:
                results.append(ConfigData(**_map_data(json.load(file))))
        for path in glob.iglob(
            os.path.join(ROOT, configpath, "configtool", "**/*.toml"), recursive=True
        ):
            with open(path, "rb") as file:
                results.append(ConfigData(**_map_data(tomllib.load(file))))

    return results


def _get_config_info(atom: Atom) -> Optional[ConfigData]:
    for data in get_config_files():
        if atom_sat(data.id, atom):
            return data
    return None


@lru_cache()
def get_vfs_dirs():
    if not env.PREFIX_NAME:
        env.set_prefix(os.environ["PORTMOD_PREFIX_NAME"])
    installed_dict = load_all_installed_map()
    installed = [mod for group in installed_dict.values() for mod in group]

    graph: Dict[Tuple[str, str, bool], Set[Tuple[str, str, bool]]] = {}
    priorities = {}
    sizes = {}

    user_config_path = os.path.join(
        os.environ["PORTMOD_CONFIG_DIR"], "config", "install.csv"
    )
    # Always case sensitive since this operates on package atoms, which are case sensitive
    # unlike the contents of the VFS
    userconfig: Dict[str, Set[str]] = read_userconfig(user_config_path, False)

    def get_directory_size(path: str):
        count = 0
        for _, _, files in os.walk(path):
            count += len(files)
        return count

    # Determine all Directories that are enabled
    for mod in installed:
        if mod._PYBUILD_VER != 1:
            continue
        for install in get_directories(mod):
            if install.VFS or install.VFS is None:
                default = os.path.normpath(install.PATCHDIR) == "."
                path = get_dir_path(mod, install)
                graph[(mod.ATOM.CP, path, default)] = set()
                priorities[(mod.CP, path, default)] = mod.TIER
                sizes[(mod.CP, path, default)] = get_directory_size(path), path

    for file in get_config_files():
        if file.path is not None:
            graph[(file.id, file.path, True)] = set()
            priorities[(file.id, file.path, True)] = file.tier
            sizes[(file.id, file.path, True)] = get_directory_size(file.path), file.path

    # Add edges in the graph for each data override
    def add_directory(ipath: str, pkg: Atom, parents: Set[Atom], idefault: bool):
        for parent in parents:
            if parent.USE and not usedep_matches_installed(parent):
                continue

            for atom, path, default in graph:
                if atom_sat(Atom(atom), parent) and default:
                    if Atom(atom).BLOCK:
                        # Blockers have reversed edges
                        graph[(pkg.CP, ipath, idefault)].add((atom, path, default))
                    else:
                        graph[(atom, path, default)].add((pkg.CP, ipath, idefault))

    for mod in installed:
        if mod._PYBUILD_VER != 1:
            continue
        for install in get_directories(mod):
            if install.VFS is False:
                continue
            idefault = os.path.normpath(install.PATCHDIR) == "."
            ipath = get_dir_path(mod, install)
            parents = set(
                use_reduce(
                    mod.DATA_OVERRIDES + " " + install.DATA_OVERRIDES,
                    mod.INSTALLED_USE,
                    flat=True,
                    token_class=Atom,
                )
            ) | {
                Atom(override)
                for name in userconfig
                for override in userconfig[name]
                if atom_sat(mod.ATOM, Atom(name))
            }
            add_directory(ipath, mod.ATOM, parents, idefault)

    for file in get_config_files():
        parents = set(Atom(token) for token in file.overrides)
        if file.path is not None:
            add_directory(file.path, file.id, parents, True)

    try:
        sorted_mods = tsort(graph, sizes, priorities)
    except CycleException as error:
        raise CycleException(
            "Encountered cycle when sorting vfs!", error.cycle
        ) from error

    return [path for _, path, _ in sorted_mods]


def get_masters(plugin: str) -> List[str]:
    _, ext = os.path.splitext(plugin.lower())
    os.stat(plugin)
    if ext in (".omwaddon", ".esp", ".esm", ".esl", ".omwgame"):
        return cast(
            str, execute(f'delta_plugin deps "{plugin}"', pipe_output=True)
        ).splitlines()
    return []


def get_plugin_size(plugin: str) -> Optional[int]:
    _, ext = os.path.splitext(plugin.lower())
    os.stat(plugin)
    if ext in (".omwaddon", ".esp", ".esm", ".esl", ".omwgame"):
        return int(execute(f'delta_plugin size "{plugin}"', pipe_output=True))
    return None


class Config:
    def __init__(
        self,
        path: str,
        typ: str,
        aux: Optional[str] = None,
        section_pattern: Optional[str] = None,
        entry_pattern: Optional[str] = None,
        section: Optional[str] = None,
        key: Optional[str] = None,
        value: Optional[str] = None,
        spaces_to_underscores: bool = False,
        ini: bool = False,
        file=None,
    ):
        self.path = os.path.expanduser(path)
        self.typ = typ
        self.aux = aux
        self.section_pattern = section_pattern
        self.entry_pattern = entry_pattern
        self.section = section
        self.key = key
        self.value = value
        self.spaces_to_underscores = spaces_to_underscores
        self.ini = ini
        self.file = file


def usedep_matches_installed(atom: Atom) -> bool:
    mod = load_installed_pkg(atom.strip_use())
    if mod and mod._PYBUILD_VER == 1:
        installed_flags = mod.INSTALLED_USE
    else:
        pkg = _get_config_info(Atom(atom.CPN))
        if pkg:
            installed_flags = pkg.flags
        else:
            return False  # If override isn't installed, it won't be in the graph

    for flag in atom.USE:
        if flag.startswith("-") and flag.lstrip("-") in installed_flags:
            return False  # Required flag is not set
        elif not flag.startswith("-") and flag not in installed_flags:
            return False  # Required flag is not set

    return True


def _config_entry_pattern(config, **kwargs):
    return _config_pattern(config, config.entry_pattern, **kwargs)


def _config_section_pattern(config, **kwargs):
    return _config_pattern(config, config.section, **kwargs)


def _config_key_pattern(config, **kwargs):
    return _config_pattern(config, config.key, **kwargs)


def _config_value_pattern(config, **kwargs):
    return _config_pattern(config, config.value, **kwargs)


def _config_pattern(config, pattern, **kwargs):
    if config.spaces_to_underscores:

        def wrapper(s: str):
            return s.replace(" ", "_")

    else:

        def wrapper(s: str):
            return s

    if "section" not in kwargs:
        kwargs["section"] = config.section
    if "key" not in kwargs:
        kwargs["key"] = config.key
    if "value" not in kwargs:
        kwargs["value"] = config.value

    pattern_ = pattern
    while any("{" + key + "}" in pattern_ for key in kwargs):
        for key in kwargs:
            # Only turn spaces into underscores for sections and keys
            if key in {"key", "section"} or (
                config.aux and key in {config.aux + "_SECTION", config.aux + "_KEY"}
            ):
                pattern_ = pattern_.replace(
                    "{" + key + "}", wrapper(str(kwargs[key])) or ""
                )
            else:
                pattern_ = pattern_.replace("{" + key + "}", str(kwargs[key]) or "")
    return re.sub("{.*?}", "*", pattern_)


def _commit_changes(config, new):
    if config.ini:
        inisection = _config_section_pattern(config)
        newentries = {}
        for index, entry in enumerate(new):
            inikey = _config_key_pattern(
                config, **{config.aux or config.typ: entry, "i": index}
            )
            inivalue = _config_value_pattern(
                config, **{config.aux or config.typ: entry, "i": index}
            )
            newentries[inikey] = inivalue

        del config.file[inisection]
        for key, value in newentries.items():
            config.file[inisection, key] = value
    else:
        remove_config(config.file, f"{config.section}=*")
        for entry in new:
            config.file.append(_config_entry_pattern(config, **{config.aux: entry}))


def read_userconfig(path: str, case_insensitive: bool) -> Dict[str, Set[str]]:
    userconfig = defaultdict(set)

    if os.path.exists(path):
        # Read user config
        with open(path, newline="") as csvfile:
            csvreader = csv.reader(csvfile, skipinitialspace=True)
            for row in csvreader:
                assert len(row) > 1
                atom = row[0].strip()
                if case_insensitive:
                    userconfig[atom.lower()] |= set(
                        map(lambda x: x.strip().lower(), row[1:])
                    )
                else:
                    userconfig[atom] |= set(map(lambda x: x.strip(), row[1:]))

    return userconfig


def get_sorted_files(key: str):
    # Sort 'content' entries in config
    # Create graph of content files that are installed, with masters of a file
    # being the parent of the node in the graph
    # Any other content files found in the config should be warned about and
    # removed.
    graph: Dict[str, Set[str]] = {}
    priorities: Dict[str, str] = {}
    sizes: Dict[str, int] = {}

    case_insensitive = bool(os.environ.get("CASE_INSENSITIVE_FILES", False))
    real_names = {}

    # Keys refer to master atoms (overridden).
    # values are a set of overriding mod atomso
    user_config_path = os.path.join(
        os.environ["PORTMOD_CONFIG_DIR"], "config", key.lower() + ".csv"
    )
    # Keys refer to masters (overridden).
    # values are a set of overriding files
    userconfig: Dict[str, Set[str]] = read_userconfig(
        user_config_path, case_insensitive
    )

    # Determine all Files that are enabled
    for pkg in load_all_installed():
        for _, file in get_files(pkg, key):
            name = file.NAME
            if case_insensitive:
                name = file.NAME.lower()
            graph[name] = set()
            priorities[name] = pkg.TIER
            real_names[name] = file.NAME

    for config_file in get_config_files():
        for file in getattr(config_file, key.lower(), []):
            name = file.name
            if case_insensitive:
                name = name.lower()
            graph[name] = set()
            priorities[name] = str(config_file.tier)
            real_names[name] = file.name

    # Validate entries in userconfig
    for entry in userconfig.keys() | {
        item for group in userconfig.values() for item in group
    }:
        if entry not in graph:
            warning(f"File {entry} in {user_config_path} could not be found!")

    for pkg in load_all_installed():
        for install, file in get_files(pkg, key):
            name = file.NAME
            if case_insensitive:
                name = file.NAME.lower()

            path = get_file_path(pkg, install, file)
            # Break ties by the lexicographically least
            sizes[name] = get_plugin_size(path) or 0, name
            # We need a path to determine masters
            masters = set(get_masters(path))
            if isinstance(file.OVERRIDES, str):
                masters |= set(use_reduce(file.OVERRIDES, pkg.INSTALLED_USE))
            else:
                masters |= set(file.OVERRIDES)

            if name in userconfig:
                masters |= set(userconfig[name])

            # Add edge from master to child
            for master in masters:
                if case_insensitive and master.lower() in graph:
                    graph[master.lower()].add(name)
                elif not case_insensitive and master in graph:
                    graph[master].add(name)

    for config_file in get_config_files():
        if config_file.path is None:
            continue
        for file in getattr(config_file, key.lower(), []):
            name = file.name
            if case_insensitive:
                name = name.lower()

            path = os.path.join(config_file.path, file.name)
            sizes[name] = get_plugin_size(path) or 0, name
            # We need a path to determine masters
            masters = set(get_masters(path))
            masters |= set(file.overrides)

            if file.name in userconfig:
                masters |= set(userconfig[file.name])

            # Add edge from master to child
            for master in masters:
                if case_insensitive and master.lower() in graph:
                    graph[master.lower()].add(name)
                elif not case_insensitive and master in graph:
                    graph[master].add(name)

    try:
        new_files = tsort(graph, sizes, priorities)
    except CycleException as e:
        raise CycleException(f"Encountered cycle when sorting {key}!", e.cycle) from e

    return [real_names[name] for name in new_files]


def sort_config(config: Config):
    """Regenerates managed sections of config files"""
    env.set_prefix(os.environ["PORTMOD_PREFIX_NAME"])

    def section_pattern(config, **kwargs):
        if "section" not in kwargs:
            kwargs["section"] = config.section
        if "key" not in kwargs:
            kwargs["key"] = config.key
        if "value" not in kwargs:
            kwargs["value"] = config.value

        pattern = config.section_pattern
        while any("{" + key + "}" in pattern for key in kwargs):
            for key in kwargs:
                pattern = pattern.replace("{" + key + "}", str(kwargs[key]) or "")
        return re.sub("{.*?}", "*", pattern)

    installed = load_all_installed()
    print(f"Sorting {config.path} {config.aux} entries...")

    if config.typ == "INSTALL":
        # Sort 'data' entries in config
        if os.environ.get("WSL_INTEROP"):
            # If using WSL, rewrite paths that reference outside WSL to work from outside WSL.
            new_dirs = [
                '"' + re.sub(r"^/mnt/([a-z])/", r"\1:/", path) + '"'
                for path in get_vfs_dirs()
            ]
        else:
            new_dirs = ['"' + path + '"' for path in get_vfs_dirs()]

        _commit_changes(config, new_dirs)
    elif config.typ == "FILE":
        assert config.aux
        new_files = get_sorted_files(config.aux)

        _commit_changes(config, new_files)
    elif config.typ == "FIELD":
        sort_field(config, installed)


def sort_field(config, installed):
    graph: Dict[Atom, Set[Atom]] = {}
    priorities: Dict[Atom, str] = {}
    field_data: Dict[Atom, Dict] = {}

    assert config.aux

    for mod in installed:
        if mod._PYBUILD_VER != 1:
            continue
        dictionary = mod.get_installed_env().get(config.aux)
        if dictionary:
            graph[mod.CP] = set()
            priorities[mod.CP] = mod.TIER
            field_data[mod.CP] = dictionary

    for config_file in get_config_files():
        dictionary = getattr(config_file, config.aux.lower(), None)
        if dictionary:
            graph[config_file.id] = set()
            priorities[config_file.id] = str(config_file.tier)
            field_data[config_file.id] = dictionary

    for mod in installed:
        if mod._PYBUILD_VER != 1:
            continue
        for parent in use_reduce(
            mod.DATA_OVERRIDES, mod.INSTALLED_USE, flat=True, token_class=Atom
        ):
            if not usedep_matches_installed(parent) or parent not in graph:
                continue

            # pylint: disable=dict-iter-missing-items
            for atom in graph:
                if atom_sat(Atom(atom), parent):
                    if Atom(atom).BLOCK:
                        # Blockers are reversed
                        graph[mod.CP].add(atom)
                    else:
                        graph[atom].add(mod.CP)

    for config_file in get_config_files():
        for parent in config_file.overrides:
            if not usedep_matches_installed(parent) or parent not in graph:
                continue

            # pylint: disable=dict-iter-missing-items
            for atom in graph:
                if atom_sat(atom, parent):
                    if atom.BLOCK:
                        # Blockers are reversed
                        graph[config_file.id].add(atom)
                    else:
                        graph[atom].add(config_file.id)

    sections = defaultdict(dict)
    for atom in tsort(graph, {node: node for node in graph}, priorities):
        for section, sectiondata in field_data[atom].items():
            sections[section].update(sectiondata)

    user_config_path = os.path.join(
        os.environ["PORTMOD_CONFIG_DIR"], "config", config.aux.lower() + ".cfg"
    )
    if os.path.exists(user_config_path):
        with open(user_config_path, encoding="utf-8") as user_file:
            user_config = INI(user_file)
        for section in user_config:
            for key in user_config.keys(section):
                value = user_config[section, key]
                if isinstance(value, list):
                    raise Exception(
                        f"Multiple values found for section {section} and key {key}. "
                        f"Multiple values are not allowed in {config.aux.lower()}.cfg!"
                    )
                sections[section][key] = value

    if not config.ini:
        pattern = re.sub("{.*}", "*", config.section)
        remove_config(config.file, pattern)
    for section in sections:
        if config.ini:
            inisection = _config_section_pattern(
                config, **{config.aux + "_SECTION": section}
            )
            for index, key in enumerate(sections[section]):
                inikey = _config_key_pattern(
                    config, **{config.aux + "_KEY": key, "i": index}
                )
                inivalue = _config_value_pattern(
                    config,
                    **{
                        config.aux + "_VALUE": str(sections[section][key]),
                        "i": index,
                    },
                )
                config.file[inisection, inikey] = inivalue
        else:
            for key in sections[section]:
                config.file.append(
                    _config_entry_pattern(
                        config,
                        **{
                            config.aux + "_SECTION": section,
                            config.aux + "_KEY": key,
                            config.aux + "_VALUE": str(sections[section][key]),
                        },
                    )
                )
