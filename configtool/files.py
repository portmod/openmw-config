# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from types import SimpleNamespace
from typing import Generator, Tuple

from portmod.globals import env
from portmod.pybuild import Pybuild
from portmodlib.fs import ci_exists
from portmodlib.globals import root
from portmodlib.pybuild import BasePybuild
from portmodlib.usestr import check_required_use


class File(SimpleNamespace):
    """Deprecated stub for portmod's old File structure"""


class InstallDir(SimpleNamespace):
    """Deprecated stub for portmod's old InstallDir structure"""


def get_directories(package: Pybuild) -> Generator[InstallDir, None, None]:
    """
    Returns all enabled InstallDir objects in INSTALL_DIRS
    """
    if getattr(package, "_PYBUILD_VER", 1) == 1:
        if not env.PREFIX_NAME:
            env.set_prefix(os.environ["PORTMOD_PREFIX_NAME"])
        for install_dir in package.INSTALL_DIRS:
            if check_required_use(
                install_dir.REQUIRED_USE, package.get_use(), package.valid_use
            ):
                yield install_dir


def get_files(
    package: Pybuild, typ: str
) -> Generator[Tuple[InstallDir, File], None, None]:
    """
    Returns all enabled files and their directories
    """
    if getattr(package, "_PYBUILD_VER", 1) == 1:
        if not env.PREFIX_NAME:
            env.set_prefix(os.environ["PORTMOD_PREFIX_NAME"])
        for install_dir in get_directories(package):
            if hasattr(install_dir, typ):
                for file in getattr(install_dir, typ):
                    if check_required_use(
                        file.REQUIRED_USE, package.get_use(), package.valid_use
                    ):
                        yield install_dir, file


# Note: This function will only work inside the sandbox, as otherwise INSTALL_DEST
# won't be an environment variable
def _get_install_dir_dest(pkg: BasePybuild):
    install_dir_dest = os.environ.get("INSTALL_DEST", ".")
    for attr in dir(pkg):
        if not attr.startswith("_") and isinstance(getattr(pkg, attr), str):
            install_dir_dest = install_dir_dest.replace(
                "{" + attr + "}", getattr(pkg, attr)
            )
    return os.path.normpath(install_dir_dest)


def get_dir_path(pkg: BasePybuild, install_dir: InstallDir) -> str:
    """Returns the installed path of the given InstallDir"""
    # Local dirs should be relative to LOCAL_MODS
    if "local" in pkg.PROPERTIES:
        from portmodlib.globals import local_mods

        path = os.path.normpath(os.path.join(local_mods(), pkg.PN))
    else:
        path = ci_exists(
            os.path.join(root(), _get_install_dir_dest(pkg), install_dir.PATCHDIR),
            prefix=root(),
        )
    if os.path.islink(path):
        return os.readlink(path)

    return path


def get_file_path(pkg: BasePybuild, install_dir: InstallDir, esp: File) -> str:
    return os.path.join(get_dir_path(pkg, install_dir), esp.NAME)
