# Configtool

The portmod module for updating game config files.

## OpenMW

This module sorts the `openmw.cfg` and `settings.cfg` files using information about installed mods from [Portmod](https://gitlab.com/portmod/portmod).

Discovery of the config files is done via the `OPENMW_CONFIG_DIR` environment variable, which can be a single path, or a `:`-separated list of paths (`;` on windows).

Configtool reads configuration files written in json or toml from `${ROOT}/${CONFIGPATH}/configtool` (subdirectories are permitted) with the following format (example is in toml):

```toml
# string, mandatory
# Must have the same format as portmod package names,
# but does not need to refer to an actual package
id = "local/my-mod"
# string, optional
# The path to be added to openmw.cfg as a data= line
# If omitted, the config file will not affect the VFS, but still can be used for adding settings/fallback entries.
path = "/full/path/to/installed/data"

# list of file data, optional
# Declares esp/esm/omwaddon/omwgame files that are to be included in the `content` section of `openmw.cfg`
plugins = [
  { name = "Foo.omwaddon", overrides = ["Some other plugin.esp"] }
]
# Same as plugins, but for groundcover files
groundcover = []
# Same as plugins, but for the fallback-archive section of openmw.cfg
archives = []

# string or number, optional
# An alphanumeric tier used as the default priority when sorting
# Can be used to create sorting groups for mods.
# The convention is as follows:
# 1. Base game data
# 2. Mods that directly replace base game data
# a. Everything else
# z. Stuff that needs to load last
tier = "a"

# A list of other mods to override, optional.
# These mods should have their own configuration file
# and can include versions and comparison operators to refer to specific versions.
overrides = ["base/morrowind", ">=cat/some-other-id-1.0"]

# Fallback data for the fallback line in openmw.cfg
# The format should be the same as in Morrowind.ini (but as a subtable of fallback)
# this maps to fallback=<section>_<key>,<value>, with spaces in the section and key being converted to underscores.
[fallback.section]
key = "value"

# Settings for openmw's settings.cfg
# The format matches settings.cfg (but as a subtable of settings)
[settings.section]
key = "value"

# List of strings, optional
# A list of configuration flags which are enabled for this mod
# Only present to allow overrides to be conditional on use flags in portmod
flags = ["foo", "bar", "baz"]
```

Additional data is pulled from installed Pybuild1-based packages using the legacy format, as described below.

### Legacy Format

Configtool expects the following portmod values to be used in build files:
- A `PLUGINS: List[File]` field in InstallDirs .
- A `GROUNDCOVER: List[File]` field in InstallDirs to declare esp/omwaddon files that are to be included in the `groundcover` section of `openmw.cfg`.
- An `ARCHIVES: List[File]` field in InstallDirs to declare bsa files that are to be included in the `fallback-archive` section of `openmw.cfg`.
- A `FALLBACK: Dict[str, Dict[str, Union[str, int, float]]]` field at the root of the Package object to declare settings to be included in the `fallback` section of `openmw.cfg`. These dictionaries should be of the form `{"section": {"field_name": "value"}}`.
- A `SETTINGS: Dict[str, Dict[str, Union[str, int, float]]]` field at the root of the Package object to declare settings to be included in `settings.cfg`. These dictionaries should be of the form `{"section": {"field_name": "value"}}`.

Note that the `data` section of `openmw.cfg` mirrors portmod's internally defined VFS (based on the contents of the INSTALL_DIRS field).

The FALLBACK field is declared as a dictionary, matching the `Morrowind.ini` structure, rather than `openmw.cfg`'s structure.

To encode a fallback line that uses `openmw.cfg`'s structure, you need to determine the section and key used by vanilla morrowind (this is only really important if support for vanilla Morrowind is ever implemented. As long as your choice of section and key produces what openmw expects, it will be fine). For openmw, spaces will be replaced by underscores, and the line will be constructed as shown below.

`openmw.cfg` fallback lines have following form:

```ini
fallback=<section>_<key>,<value>
```
As some keys and sections contain spaces/underscores, the way this is constructed may be ambiguous, however it should not affect how the result is encoded (though it may affect how it overrides values from other packages).

## Bethesda Game Engines

E.g. Fallout: New Vegas, Oblivion (others have not been tested)

The module sorts the `Plugins.txt` file. The path for the file can be configured using the `PLUGINS_TXT_PATH` variable.
It can also configure `ini` files to be updated to include data set in packages' `SETTINGS` variable. The path for the `ini` file can be configured with `INI_PATH`

Format: See OpenMW, but groundcover and fallback don't do anything and should not be used.

## Legacy Format

It expects the following portmod values to be used in build files:
- A `PLUGINS: List[File]` field in InstallDirs to declare esp/esm/esl files that are to be included in `Plugins.txt`.
- A `SETTINGS: Dict[str, Dict[str, Union[str, int, float]]]]` field of the Package class to declare settings to be included in the `ini` file. Should have the form `{"section": {"key": "value"}}`.

Optionally, as it's required for some games, if the `TIMESTAMP_PLUGINS` environment variable is set to any nonempty string, it will also sort the plugin files by their timestamps. `PLUGINS_TXT_FILE_PREFIX` may also be set to prepend the specified text to each `Plugins.txt` entry. This is useful in Fallout 4, as it requires a `*` in front of an entry to enable it.

### VFS

It will also add dummy plugins, as created by `common/bsa` from the [BSA repository](https://gitlab.com/portmod/bsa), which set up the VFS. These dummy plugins are tied to BSA files created to form a VFS with files which would otherwise be installed loosely. As such, their plugins are loaded last, as usually it is expected that loose files override BSAs (no archive invalidation required in this case).

The order of these plugins/archives can be influenced using a package's `DATA_OVERRIDES` and `TIER`, in the same manner as a portmod profile which uses the builtin VFS, in addition to [User Sorting Rules](https://gitlab.com/portmod/portmod/-/wikis/Configuration/User-Sorting-Rules).

## Dependencies

configtool depends on `delta_plugin` 0.16 or newer to parse the master list and record counts from esp/esm/esl/omwaddon/omwgame files.
